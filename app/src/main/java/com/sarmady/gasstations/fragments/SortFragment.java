package com.sarmady.gasstations.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sarmady.gasstations.R;
import com.sarmady.gasstations.activities.FilterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SortFragment extends Fragment {

    @BindView(R.id.rg_sort)
    RadioGroup mSortGroup;
    @BindView(R.id.rb_nearest)
    RadioButton mNearestRadioButton;
    @BindView(R.id.rb_price_highest)
    RadioButton mPricehighestRadioButton;
    @BindView(R.id.rb_price_lowest)
    RadioButton mPricelowestRadioButton;
    @BindView(R.id.rb_stars_highest)
    RadioButton mStarsHighestRadioButton;
    @BindView(R.id.rb_stars_lowest)
    RadioButton mStarsLowestRadioButton;
    @BindView(R.id.rb_names_a_z)
    RadioButton mNamesAZRadioButton;
    @BindView(R.id.rb_names_z_a)
    RadioButton mNamesZARadioButton;

    FilterActivity mFilterActivity;

    public SortFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sort, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFilterActivity = new FilterActivity();
        checkedChangeListener();
        selectPreferences();
    }

    private void checkedChangeListener() {
        mSortGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rb_nearest:
                    mFilterActivity.setmSort("nearest");
                    break;
                case R.id.rb_price_highest:
                    mFilterActivity.setmSort("price_highest");
                    break;
                case R.id.rb_price_lowest:
                    mFilterActivity.setmSort("price_lowest");
                    break;
                case R.id.rb_stars_highest:
                    mFilterActivity.setmSort("stars_highest");
                    break;
                case R.id.rb_stars_lowest:
                    mFilterActivity.setmSort("stars_lowest");
                    break;
                case R.id.rb_names_a_z:
                    mFilterActivity.setmSort("names_a_z");
                    break;
                case R.id.rb_names_z_a:
                    mFilterActivity.setmSort("names_z_a");
                    break;
            }
        });
    }

    private void selectPreferences() {
        switch (mFilterActivity.getmSort()) {
            case "nearest":
        mNearestRadioButton.setChecked(true);
                break;
            case "price_highest":
                mPricehighestRadioButton.setChecked(true);
                break;
            case "price_lowest":
                mPricelowestRadioButton.setChecked(true);
                break;
            case "stars_highest":
                mStarsHighestRadioButton.setChecked(true);
                break;
            case "stars_lowest":
                mStarsLowestRadioButton.setChecked(true);
                break;
            case "names_a_z":
                mNamesAZRadioButton.setChecked(true);
                break;
            case "names_z_a":
                mNamesZARadioButton.setChecked(true);
                break;
        }
    }
}
