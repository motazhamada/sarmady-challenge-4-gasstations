package com.sarmady.gasstations.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.sarmady.gasstations.R;
import com.sarmady.gasstations.activities.FilterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.cb_fuel_gas_91)
    CheckBox mFuelTypeGas91;
    @BindView(R.id.cb_fuel_gas_95)
    CheckBox mFuelTypeGas95;
    @BindView(R.id.cb_fuel_diesel)
    CheckBox mFuelTypeDiesel;
    @BindView(R.id.cb_features_restaurant)
    CheckBox mFeaturesRestaurant;
    @BindView(R.id.cb_features_mosque)
    CheckBox mFeaturesMosque;
    @BindView(R.id.cb_features_coffee)
    CheckBox mFeaturesCoffe;
    @BindView(R.id.cb_features_toilet_female)
    CheckBox mFeaturesToiletFemale;
    @BindView(R.id.cb_features_toilet_male)
    CheckBox mFeaturesToiletMale;
    @BindView(R.id.cb_features_hotel)
    CheckBox mFeaturesHotel;
    @BindView(R.id.cb_features_atm)
    CheckBox mFeaturesATM;

    FilterActivity mFilterActivity;

    public FilterFragment() {
        // Required empty public constructor
    }

    public static FilterFragment newInstance() {
        return new FilterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mFilterActivity = new FilterActivity();

        checkedChangeListener();
        selectPreferences();

    }

    private void checkedChangeListener() {
        mFuelTypeGas91.setOnCheckedChangeListener(this);
        mFuelTypeGas95.setOnCheckedChangeListener(this);
        mFuelTypeDiesel.setOnCheckedChangeListener(this);
        mFeaturesRestaurant.setOnCheckedChangeListener(this);
        mFeaturesMosque.setOnCheckedChangeListener(this);
        mFeaturesCoffe.setOnCheckedChangeListener(this);
        mFeaturesToiletFemale.setOnCheckedChangeListener(this);
        mFeaturesToiletMale.setOnCheckedChangeListener(this);
        mFeaturesHotel.setOnCheckedChangeListener(this);
        mFeaturesATM.setOnCheckedChangeListener(this);
    }
    private void selectPreferences() {
        if (mFilterActivity.ismFuelTypeGas91()){
            mFuelTypeGas91.setChecked(true);
        }
        if (mFilterActivity.ismFuelTypeGas95()){
            mFuelTypeGas95.setChecked(true);
        }
        if (mFilterActivity.ismFuelTypeDiesel()){
            mFuelTypeDiesel.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesRestaurant()){
            mFeaturesRestaurant.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesMosque()){
            mFeaturesMosque.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesCoffee()){
            mFeaturesCoffe.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesFemaleToilet()){
            mFeaturesToiletFemale.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesMaleToilet()){
            mFeaturesToiletMale.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesHotel()){
            mFeaturesHotel.setChecked(true);
        }
        if (mFilterActivity.ismFeaturesATM()){
            mFeaturesATM.setChecked(true);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_fuel_gas_91:
                mFilterActivity.setmFuelTypeGas91(isChecked);
                break;
            case R.id.cb_fuel_gas_95:
                mFilterActivity.setmFuelTypeGas95(isChecked);
                break;
            case R.id.cb_fuel_diesel:
                mFilterActivity.setmFuelTypeDiesel(isChecked);
                break;
            case R.id.cb_features_restaurant:
                mFilterActivity.setmFeaturesRestaurant(isChecked);
                break;
            case R.id.cb_features_mosque:
                mFilterActivity.setmFeaturesMosque(isChecked);
                break;
            case R.id.cb_features_coffee:
                mFilterActivity.setmFeaturesCoffee(isChecked);
                break;
            case R.id.cb_features_toilet_female:
                mFilterActivity.setmFeaturesFemaleToilet(isChecked);
                break;
            case R.id.cb_features_toilet_male:
                mFilterActivity.setmFeaturesMaleToilet(isChecked);
                break;
            case R.id.cb_features_hotel:
                mFilterActivity.setmFeaturesHotel(isChecked);
                break;
            case R.id.cb_features_atm:
                mFilterActivity.setmFeaturesATM(isChecked);
                break;
        }
    }
}
