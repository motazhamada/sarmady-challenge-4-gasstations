package com.sarmady.gasstations.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sarmady.gasstations.R;
import com.sarmady.gasstations.activities.StationDetailsActivity;
import com.sarmady.gasstations.classes.GasStation;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class GasStationsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private List<GasStation> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private Handler handler = new Handler();

    // data is passed into the constructor
    public GasStationsRecyclerViewAdapter(Context context, List<GasStation> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = mInflater.inflate(R.layout.item_gas_station, parent, false);
        ButterKnife.bind(this, view);
        return new ViewHolder(view);
    }

    public void add(GasStation item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void add(int position, GasStation item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void addAll(List<GasStation> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    public void clearAll() {
        handler.removeCallbacksAndMessages(null);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final GasStation mGasStation = mData.get(position);
        try {
            final ViewHolder viewHolder = (ViewHolder) holder;
            loadData(mGasStation, viewHolder);
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "GasStationsRecyclerViewAdapter 1 = " + String.valueOf(e));
        }
    }

    private void loadData(final GasStation mGasStation, ViewHolder viewHolder) {
        setTexts(mGasStation, viewHolder);
        setIcons(mGasStation, viewHolder);
        viewHolder.mRateRatingBar.setRating(mGasStation.getRating());
        Picasso.get()
                .load(mGasStation.getCompany().get_image().getUrl())
                .fit()
                .placeholder(R.drawable.ic_station_marker)
                .error(R.drawable.ic_station_marker)
                .into(viewHolder.mLogoCircleImageView);
        viewHolder.mDirectionImageButton.setOnClickListener(v -> {
            String url = "http://maps.google.com/maps?daddr=" + mGasStation.getPosition().getLat() + "," + mGasStation.getPosition().getLng();
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url));
            mContext.startActivity(intent);
        });
        viewHolder.mGasStationConstrainLayout.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, StationDetailsActivity.class);
            intent.putExtra("StationTitle", mGasStation.getTitle());
            intent.putExtra("StationAddress", mGasStation.getAddress());
            intent.putExtra("StationLat", mGasStation.getPosition().getLat());
            intent.putExtra("StationLng", mGasStation.getPosition().getLng());
            intent.putExtra("StationUrl", mGasStation.getCompany().get_image().getUrl());
            mContext.startActivity(intent);
        });

    }

    private void setIcons(GasStation mGasStation, ViewHolder viewHolder) {
        if (mGasStation.isHasATM()) {
            viewHolder.mATMImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mATMImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasCafe()) {
            viewHolder.mCoffeImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mCoffeImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasHotel()) {
            viewHolder.mHotelImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mHotelImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasLadiesWC()) {
            viewHolder.mToiletFemaleImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mToiletFemaleImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasMensWC()) {
            viewHolder.mToiletmaleImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mToiletmaleImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasMasjid()) {
            viewHolder.mMosqueImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mMosqueImageView.setVisibility(View.GONE);
        }
        if (mGasStation.isHasRestaurant()) {
            viewHolder.mRestaurantImageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mRestaurantImageView.setVisibility(View.GONE);
        }
    }

    private void setTexts(GasStation mGasStation, ViewHolder viewHolder) {
        viewHolder.mNameTextView.setText(String.valueOf(mGasStation.getTitle()));
        viewHolder.mAddressTextView.setText(mGasStation.getAddress());
        viewHolder.m1GasPriceTextView.setText(String.format("%s %s", String.valueOf(mGasStation.getGas91Price()), mGasStation.getCurrency().getEnCurrency()));
        viewHolder.m2GasPriceTextView.setText(String.format("%s %s", String.valueOf(mGasStation.getGas95Price()), mGasStation.getCurrency().getEnCurrency()));
        viewHolder.m3GasPriceTextView.setText(String.format("%s %s", String.valueOf(mGasStation.getDieselPrice()), mGasStation.getCurrency().getEnCurrency()));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public GasStation getItem(int id) {
        return mData.get(id);
    }

    @Override
    public void onClick(View v) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ibtn_station_address)
        ImageButton mDirectionImageButton;
        @BindView(R.id.civ_station_logo)
        CircleImageView mLogoCircleImageView;
        @BindView(R.id.tv_station_name)
        TextView mNameTextView;
        @BindView(R.id.tv_station_address)
        TextView mAddressTextView;
        @BindView(R.id.tv_1_gas_name)
        TextView m1GasNameTextView;
        @BindView(R.id.tv_1_gas_price)
        TextView m1GasPriceTextView;
        @BindView(R.id.tv_2_gas_name)
        TextView m2GasNameTextView;
        @BindView(R.id.tv_2_gas_price)
        TextView m2GasPriceTextView;
        @BindView(R.id.tv_3_gas_name)
        TextView m3GasNameTextView;
        @BindView(R.id.tv_3_gas_price)
        TextView m3GasPriceTextView;
        @BindView(R.id.iv_restaurant_ico)
        ImageView mRestaurantImageView;
        @BindView(R.id.iv_hotel_ico)
        ImageView mHotelImageView;
        @BindView(R.id.iv_atm_ico)
        ImageView mATMImageView;
        @BindView(R.id.iv_coffee_ico)
        ImageView mCoffeImageView;
        @BindView(R.id.iv_mosque_ico)
        ImageView mMosqueImageView;
        @BindView(R.id.iv_toilet_female_ico)
        ImageView mToiletFemaleImageView;
        @BindView(R.id.iv_toilet_male_ico)
        ImageView mToiletmaleImageView;
        @BindView(R.id.rb_station_rate)
        RatingBar mRateRatingBar;
        @BindView(R.id.cl_gas_station)
        ConstraintLayout mGasStationConstrainLayout;
        ViewHolder(View v1) {
            super(v1);
            ButterKnife.bind(this, v1);
        }
    }
}