package com.sarmady.gasstations.classes;

public class Company {
    private String name;
    private String id;
    private Image _image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image get_image() {
        return _image;
    }

    public void set_image(Image _image) {
        this._image = _image;
    }
}
