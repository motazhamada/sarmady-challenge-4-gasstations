package com.sarmady.gasstations.activities;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;
import com.sarmady.gasstations.R;
import com.sarmady.gasstations.classes.DataParser;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class StationDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int DEFAULT_ZOOM = 15;
    @BindView(R.id.adview_mpu)
    AdView mMPUAdView;
    @BindView(R.id.civ_station_logo)
    CircleImageView mLogoCircleImageView;
    @BindView(R.id.tv_station_address)
    TextView mAddressTextView;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;
    private SupportMapFragment mapView;
    private GoogleMap mGoogleMap;
    private Location mDefaultLocation;
    private BitmapDescriptor icon;
    private float mDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_details);
        ButterKnife.bind(this);
        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_station_marker);
        mDefaultLocation = new Location("Sarmady");
        mDefaultLocation.setLatitude(30.054799);
        mDefaultLocation.setLongitude(31.203546);

        mDistance = 0;

        Initialize_Map();
        Initialize_Ad();
    }

    private void Initialize_Ad() {
        MobileAds.initialize(StationDetailsActivity.this,
                getString(R.string.app_ad_id));

        AdRequest adRequest = new AdRequest.Builder().addTestDevice("C47FB4C7CA3EC78196EB786392DE443C").build();
        mMPUAdView.loadAd(adRequest);
    }

    private void Initialize_Map() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mapView = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if (getIntent().getStringExtra("StationTitle") == null
                || getIntent().getStringExtra("StationAddress") == null
                || getIntent().getStringExtra("StationUrl") == null) {
            return;
        }
        mAddressTextView.setText(getIntent().getStringExtra("StationAddress"));
        Picasso.get()
                .load(getIntent().getStringExtra("StationUrl"))
                .fit()
                .placeholder(R.drawable.ic_station_marker)
                .error(R.drawable.ic_station_marker)
                .into(mLogoCircleImageView);


        Location mLocationA;
        mLocationA = new Location("StationLocation");
        mLocationA.setLatitude(getIntent().getDoubleExtra("StationLat", 30.054799));
        mLocationA.setLongitude(getIntent().getDoubleExtra("StationLng", 31.203546));


        mDistance = mDefaultLocation.distanceTo(mLocationA);

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents_details,
                        findViewById(R.id.map_view), false);

                ImageView mLogoImageView = infoWindow.findViewById(R.id.logo);
                Picasso.get()
                        .load(getIntent().getStringExtra("StationUrl"))
                        .fit()
                        .placeholder(R.drawable.ic_station_marker)
                        .error(R.drawable.ic_station_marker)
                        .into(mLogoImageView);

                TextView title = infoWindow.findViewById(R.id.title);
                title.setText(marker.getTitle());


                TextView snippet = infoWindow.findViewById(R.id.snippet);
                snippet.setText(marker.getSnippet());
                return infoWindow;
            }

        });

        getStationLocation();
        getDeviceLocation();

        String url = getUrl(mDefaultLocation,mLocationA);
        Log.d("onMapClick", url.toString());
        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDefaultLocation.getLatitude(),mDefaultLocation.getLongitude())));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void getStationLocation() {
        mGoogleMap.addMarker(new MarkerOptions()
                .title(getIntent().getStringExtra("StationTitle")).icon(icon)
                .position(
                        new LatLng(getIntent().getDoubleExtra("StationLat", 30.054799),
                                getIntent().getDoubleExtra("StationLng", 31.203546)))
                .snippet(getIntent().getStringExtra("StationAddress")));
//        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                new LatLng(getIntent().getDoubleExtra("StationLat", 30.054799),
//                        getIntent().getDoubleExtra("StationLng", 31.203546)), DEFAULT_ZOOM));
    }

    private void getDeviceLocation() {
        try {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // Get the current location of the device and set the position of the map.
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.getResult();
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mLastKnownLocation.getLatitude(),
                                        mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        mDefaultLocation.setLatitude(mLastKnownLocation.getLatitude());
                        mDefaultLocation.setLongitude(mLastKnownLocation.getLongitude());
                        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                        mGoogleMap.addMarker(new MarkerOptions()
                                .title("My Location")
                                .position(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()))
                                .snippet("this is my location now"));
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }
    private String getUrl(Location origin, Location dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude()+ "," + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude()+ "," + dest.getLongitude();


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }
    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//
//
//        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//
//            @Override
//            public void onMapClick(LatLng point) {
//
//                // Already two locations
//                if (MarkerPoints.size() > 1) {
//                    MarkerPoints.clear();
//                    mMap.clear();
//                }
//
//                // Adding new item to the ArrayList
//                MarkerPoints.add(point);
//
//                // Creating MarkerOptions
//                MarkerOptions options = new MarkerOptions();
//
//                // Setting the position of the marker
//                options.position(point);
//
//                /**
//                 * For the start location, the color of marker is GREEN and
//                 * for the end location, the color of marker is RED.
//                 */
//                if (MarkerPoints.size() == 1) {
//                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                } else if (MarkerPoints.size() == 2) {
//                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                }
//
//
//                // Add new marker to the Google Map Android API V2
//                mMap.addMarker(options);
//
//                // Checks, whether start and end locations are captured
//                if (MarkerPoints.size() >= 2) {
//                    LatLng origin = MarkerPoints.get(0);
//                    LatLng dest = MarkerPoints.get(1);
//
//                    // Getting URL to the Google Directions API
//
//                }
//
//            }
//        });
//
//    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);

                Log.d("onPostExecute","onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                mGoogleMap.addPolyline(lineOptions);
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }

}
