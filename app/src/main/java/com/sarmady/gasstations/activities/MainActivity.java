package com.sarmady.gasstations.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sarmady.gasstations.R;
import com.sarmady.gasstations.adapters.GasStationsRecyclerViewAdapter;
import com.sarmady.gasstations.classes.GasStation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final int DEFAULT_ZOOM = 15;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    @BindView(R.id.ibtn_expand)
    ImageButton mExpandImageButton;
    @BindView(R.id.rv_gas_stations)
    RecyclerView mGasStationsRecyclerView;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.appbar)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.tb_main)
    Toolbar myToolbar;
    private Location mDefaultLocation;
    private boolean isDeviceLocationSetted = false;
    private boolean isExpanded = false;
    private int mMapViewHeight = 0;
    private SupportMapFragment mapView;
    private GoogleMap mGoogleMap;
    private CameraPosition mCameraPosition;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private List<GasStation> mGasStations;
    private List<GasStation> mGasStationsFiltered;
    private GasStationsRecyclerViewAdapter mGasStationsRecyclerViewAdapter;
    private FilterActivity mFilterActivity;
    private BitmapDescriptor icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initializeToolbar();


        mFilterActivity = new FilterActivity();

        mDefaultLocation = new Location("Sarmady");
        mDefaultLocation.setLatitude(30.054799);
        mDefaultLocation.setLongitude(31.203546);

        icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_station_marker);

        Check_GPS();
        handleMapScrolling(mAppBarLayout);

        mExpandImageButton.setOnClickListener(this);

    }

    public int compareByNearest(GasStation a, GasStation b) {
        Location mLocationA;
        mLocationA = new Location("MyLocation");
        mLocationA.setLatitude(a.getPosition().getLat());
        mLocationA.setLongitude(a.getPosition().getLng());

//                        GasStation b = (GasStation) o;
        Location mLocationB;
        mLocationB = new Location("Destination");
        mLocationB.setLatitude(b.getPosition().getLat());
        mLocationB.setLongitude(b.getPosition().getLng());

        int aDist = (int) mDefaultLocation.distanceTo(mLocationA);
        int bDist = (int) mDefaultLocation.distanceTo(mLocationB);

        return aDist - bDist;
    }

    private void mSortBy(String sortBy) {
        switch (sortBy) {
            case "nearest":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, this::compareByNearest);
                }
                Collections.sort(mGasStations, this::compareByNearest);
                break;
            case "price_highest":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> Double.compare(l1.getGas91Price(), l2.getGas91Price()));
                    Collections.reverse(mGasStationsFiltered);
                }
                Collections.sort(mGasStations, (l1, l2) -> Double.compare(l1.getGas91Price(), l2.getGas91Price()));
                Collections.reverse(mGasStations);
                break;
            case "price_lowest":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> Double.compare(l1.getGas91Price(), l2.getGas91Price()));
                }
                Collections.sort(mGasStations, (l1, l2) -> Double.compare(l1.getGas91Price(), l2.getGas91Price()));
                break;
            case "stars_highest":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> Integer.compare(l1.getRating(), l2.getRating()));
                    Collections.reverse(mGasStationsFiltered);
                }
                Collections.sort(mGasStations, (l1, l2) -> Integer.compare(l1.getRating(), l2.getRating()));
                Collections.reverse(mGasStations);
                break;
            case "stars_lowest":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> Integer.compare(l1.getRating(), l2.getRating()));
                }
                Collections.sort(mGasStations, (l1, l2) -> Integer.compare(l1.getRating(), l2.getRating()));

                break;
            case "names_a_z":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> l1.getTitle().compareTo(l2.getTitle()));
                }
                Collections.sort(mGasStations, (l1, l2) -> l1.getTitle().compareTo(l2.getTitle()));
                break;
            case "names_z_a":
                if (mGasStationsFiltered != null) {
                    Collections.sort(mGasStationsFiltered, (l1, l2) -> l1.getTitle().compareTo(l2.getTitle()));
                    Collections.reverse(mGasStations);
                }
                Collections.sort(mGasStations, (l1, l2) -> l1.getTitle().compareTo(l2.getTitle()));
                Collections.reverse(mGasStations);
                break;
        }
        mGasStationsRecyclerViewAdapter.notifyDataSetChanged();
    }

    private void filterStations() {
        mGasStationsFiltered = new ArrayList<>();
        for (GasStation g : mGasStations) {
            if (mFilterActivity.ismFuelTypeGas91()) {
                if (g.isGas91Available()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFuelTypeGas95()) {
                if (g.isGas95Available()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFuelTypeDiesel()) {
                if (g.isDieselAvailable()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesRestaurant()) {
                if (g.isHasRestaurant()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesMosque()) {
                if (g.isHasMasjid()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesFemaleToilet()) {
                if (g.isHasLadiesWC()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesMaleToilet()) {
                if (g.isHasMensWC()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesCoffee()) {
                if (g.isHasCafe()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesHotel()) {
                if (g.isHasHotel()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }
            if (mFilterActivity.ismFeaturesATM()) {
                if (g.isHasATM()) {
                    mGasStationsFiltered.add(g);
                    continue;
                }
            }

        }
        if (mGasStationsFiltered.size() > 0) {
            mGasStationsRecyclerViewAdapter = new GasStationsRecyclerViewAdapter(MainActivity.this, mGasStationsFiltered);
            mGasStationsRecyclerView.setAdapter(mGasStationsRecyclerViewAdapter);
            mGoogleMap.clear();
//            mGoogleMap.addMarker(new MarkerOptions().title("My Location")
//                    .position(
//                            new LatLng(mDefaultLocation.getLatitude(),
//                                    mDefaultLocation.getLongitude()))
//                    .snippet("this is my location now"));
            for (GasStation g : mGasStationsFiltered) {
                mGoogleMap.addMarker(new MarkerOptions()
                        .title(g.getTitle()).icon(icon)
                        .position(
                                new LatLng(g.getPosition().getLat(),
                                        g.getPosition().getLng()))
                        .snippet(g.getAddress()));
            }
        }
        mSortBy(mFilterActivity.getmSort());
    }

    private void initializeToolbar() {
        setSupportActionBar(myToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sort_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_filter:
                Intent intent = new Intent(MainActivity.this, FilterActivity.class);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void Initialize_RecyclerView() {
//        mGasStations = new ArrayList<>();
        try {
            Gson gson = new Gson();
            mGasStations = gson.fromJson(loadJSONFromAsset(), new TypeToken<ArrayList<GasStation>>() {
            }.getType());
            mGasStationsRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            mGasStationsRecyclerViewAdapter = new GasStationsRecyclerViewAdapter(MainActivity.this, mGasStations);
            mGasStationsRecyclerView.setAdapter(mGasStationsRecyclerViewAdapter);

            for (GasStation g : mGasStations) {
                mGoogleMap.addMarker(new MarkerOptions()
                        .title(g.getTitle()).icon(icon)
                        .position(
                                new LatLng(g.getPosition().getLat(),
                                        g.getPosition().getLng()))
                        .snippet(g.getAddress()));
            }
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), e.getMessage());
        }
    }

    private void handleMapScrolling(AppBarLayout appBarLayout) {
        if (appBarLayout.getLayoutParams() != null) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
            AppBarLayout.Behavior appBarLayoutBehaviour = new AppBarLayout.Behavior();
            appBarLayoutBehaviour.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
            layoutParams.setBehavior(appBarLayoutBehaviour);
        }
    }

    private void Check_GPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            Initialize_Map();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void Initialize_Map() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mapView = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        mapView.getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mGoogleMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mGoogleMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    private void getDeviceLocation() {
        try {
            if (isDeviceLocationSetted)
                return;

            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (mLocationPermissionGranted) {
                if (Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    // Get the current location of the device and set the position of the map.
                    Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                    locationResult.addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            mDefaultLocation.setLatitude(mLastKnownLocation.getLatitude());
                            mDefaultLocation.setLongitude(mLastKnownLocation.getLongitude());
                            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                            mGoogleMap.addMarker(new MarkerOptions()
                                    .title("My Location")
                                    .position(
                                            new LatLng(mLastKnownLocation.getLatitude(),
                                                    mLastKnownLocation.getLongitude()))
                                    .snippet("this is my location now"));
                            isDeviceLocationSetted = true;
                        }
//                             else {
//                                Log.d(getString(R.string.TAG), "Current location is null. Using defaults.");
//                                Log.e(getString(R.string.TAG), "Exception: %s", task.getException());
//                                mGoogleMap.moveCamera(CameraUpdateFactory
//                                        .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
//                                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
//                                mGoogleMap.addMarker(new MarkerOptions()
//                                        .title("Default Location")
//                                        .position(mDefaultLocation)
//                                        .snippet("No places found, because location permission is disabled."));
//
//                            }
                    });
                }
//                else {
//                    mGoogleMap.moveCamera(CameraUpdateFactory
//                            .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
////                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
//                    mGoogleMap.addMarker(new MarkerOptions()
//                            .title("Default Location")
//                            .position(mDefaultLocation)
//                            .snippet("No places found, because location permission is disabled."));
//
//
//                }
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Check_GPS();
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mGoogleMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mGoogleMap.setMyLocationEnabled(false);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {


            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        findViewById(R.id.map_view), false);

                TextView title = infoWindow.findViewById(R.id.title);
                title.setText(marker.getTitle());


                TextView snippet = infoWindow.findViewById(R.id.snippet);
                snippet.setText(marker.getSnippet());
                return infoWindow;
            }

        });

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();
        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        Initialize_RecyclerView();

        if (getIntent().getBooleanExtra("filter", false)) {
            filterStations();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("fuel_stations.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_expand:
                if (!isExpanded) {
                    ViewGroup.LayoutParams params = Objects.requireNonNull(mapView.getView()).getLayoutParams();
                    mMapViewHeight = params.height;
                    params.height = 2000;
                    mapView.getView().setLayoutParams(params);
                    isExpanded = true;
                } else {
                    ViewGroup.LayoutParams params = Objects.requireNonNull(mapView.getView()).getLayoutParams();
                    params.height = mMapViewHeight;
                    mapView.getView().setLayoutParams(params);
                    isExpanded = false;
                }
                break;
        }
    }
}
