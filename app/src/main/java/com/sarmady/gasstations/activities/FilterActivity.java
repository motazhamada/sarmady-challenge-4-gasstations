package com.sarmady.gasstations.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.sarmady.gasstations.R;
import com.sarmady.gasstations.fragments.FilterFragment;
import com.sarmady.gasstations.fragments.SortFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterActivity extends AppCompatActivity {
    private static String mSort = "";
    private static boolean mFuelTypeGas91 = false;
    private static boolean mFuelTypeGas95 = false;
    private static boolean mFuelTypeDiesel = false;
    private static boolean mFeaturesRestaurant = false;
    private static boolean mFeaturesMosque = false;
    private static boolean mFeaturesCoffee = false;
    private static boolean mFeaturesFemaleToilet = false;
    private static boolean mFeaturesMaleToilet = false;
    private static boolean mFeaturesHotel = false;
    private static boolean mFeaturesATM = false;
    @BindView(R.id.tabs_sort_filter)
    TabLayout mTabsTabLayout;
    @BindView(R.id.viewpager_sort_filter)
    ViewPager mViewPager;
    @BindView(R.id.btn_update_filter)
    Button mUpdateButton;

    public String getmSort() {
        return mSort;
    }

    public void setmSort(String mSort) {
        FilterActivity.mSort = mSort;
    }

    public boolean ismFuelTypeGas91() {
        return mFuelTypeGas91;
    }

    public void setmFuelTypeGas91(boolean mFuelTypeGas91) {
        FilterActivity.mFuelTypeGas91 = mFuelTypeGas91;
    }

    public boolean ismFuelTypeGas95() {
        return mFuelTypeGas95;
    }

    public void setmFuelTypeGas95(boolean mFuelTypeGas95) {
        FilterActivity.mFuelTypeGas95 = mFuelTypeGas95;
    }

    public boolean ismFuelTypeDiesel() {
        return mFuelTypeDiesel;
    }

    public void setmFuelTypeDiesel(boolean mFuelTypeDiesel) {
        FilterActivity.mFuelTypeDiesel = mFuelTypeDiesel;
    }

    public boolean ismFeaturesRestaurant() {
        return mFeaturesRestaurant;
    }

    public void setmFeaturesRestaurant(boolean mFeaturesRestaurant) {
        FilterActivity.mFeaturesRestaurant = mFeaturesRestaurant;
    }

    public boolean ismFeaturesMosque() {
        return mFeaturesMosque;
    }

    public void setmFeaturesMosque(boolean mFeaturesMosque) {
        FilterActivity.mFeaturesMosque = mFeaturesMosque;
    }

    public boolean ismFeaturesCoffee() {
        return mFeaturesCoffee;
    }

    public void setmFeaturesCoffee(boolean mFeaturesCoffee) {
        FilterActivity.mFeaturesCoffee = mFeaturesCoffee;
    }

    public boolean ismFeaturesFemaleToilet() {
        return mFeaturesFemaleToilet;
    }

    public void setmFeaturesFemaleToilet(boolean mFeaturesFemaleToilet) {
        FilterActivity.mFeaturesFemaleToilet = mFeaturesFemaleToilet;
    }

    public boolean ismFeaturesMaleToilet() {
        return mFeaturesMaleToilet;
    }

    public void setmFeaturesMaleToilet(boolean mFeaturesMaleToilet) {
        FilterActivity.mFeaturesMaleToilet = mFeaturesMaleToilet;
    }

    public boolean ismFeaturesHotel() {
        return mFeaturesHotel;
    }

    public void setmFeaturesHotel(boolean mFeaturesHotel) {
        FilterActivity.mFeaturesHotel = mFeaturesHotel;
    }

    public boolean ismFeaturesATM() {
        return mFeaturesATM;
    }

    public void setmFeaturesATM(boolean mFeaturesATM) {
        FilterActivity.mFeaturesATM = mFeaturesATM;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

        setupViewPager();
        // Set Tabs inside Toolbar
        mTabsTabLayout.setupWithViewPager(mViewPager);

        mUpdateButton.setOnClickListener(v -> {
            Intent intent =new Intent(FilterActivity.this,MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("filter",true);
            startActivity(intent);
        });

    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SortFragment(), "Sort");
        adapter.addFragment(new FilterFragment(), "Filter");
        mViewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
