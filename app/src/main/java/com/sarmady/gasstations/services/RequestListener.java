package com.sarmady.gasstations.services;

public interface RequestListener {

    void onSuccess(Object object);
//    void onFailure(int errorCode,int id);
    void onFailure(Exception error,int id);

}
